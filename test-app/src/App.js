import React, { Component } from 'react';
import { Layout } from 'antd';
import { BrowserRouter } from 'react-router-dom'
import ReactDOM from 'react-dom'

import './App.css';
import Head from './components/Header/header'
import Sidebar from './components/Sidebar/sidebar'
import Contentbox from './components/Content/content'

class App extends Component{
  render () {
    return (
      <div className="App">
        <Layout>
          <Head/>
          <Layout>
            <Sidebar/>
            <Contentbox/>
          </Layout>
        </Layout>
      </div>
    );
  };
}

export default App;
