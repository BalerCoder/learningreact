import React, { Component } from 'react';
import { Layout,Menu } from 'antd';

import './header.css';

const { Header } = Layout;

class Head extends Component{
    render() {
        return (
            <Header className="header">
                <div className="menu-bar">
                    <Menu theme="dark" mode="horizontal">
                        <Menu.Item key="1">Link 1</Menu.Item>
                        <Menu.Item key="2">Link 2</Menu.Item>
                        <Menu.Item key="3">Link 3</Menu.Item>
                        <Menu.Item key="4">Link 4</Menu.Item>
                        <Menu.Item key="5">Link 5</Menu.Item>
                    </Menu>
                </div>
            </Header>
        );
    };
}

export default Head;