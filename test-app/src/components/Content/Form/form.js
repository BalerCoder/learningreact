import React, { Component } from 'react';
import { Row, Col,Form,Input, Button } from 'antd';

import './form.css';

class RequestForm extends Component{

    onChange = (pagination, filters, sorter, extra) => {
        console.log('params', pagination, filters, sorter, extra);
    }
    
    onFinish = values => {
        console.log('Success:', values);
    };
      
    onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    render() {
        return (
            <div className="site-layout-background" style={{ padding: 24, minHeight: '100%' }}>
              <h1>Heading</h1>
              <div>
                <Form
                  name="basic"
                  initialValues={{
                    remember: true,
                  }}
                  onFinish={this.onFinish}
                  onFinishFailed={this.onFinishFailed}
                >
                  <Row>
                    <Col span={12}>
                      <Form.Item
                        label="Label"
                        name="demoinput"
                        rules={[
                          {
                            required: true,
                            message: 'Please input anything you like!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        label="Label"
                        name="demoinput"
                        rules={[
                          {
                            required: true,
                            message: 'Please input anything you like!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col span={6}></Col>
                  </Row>
                  <Row>
                    <Col span={12}>
                      <Form.Item
                        label="Label"
                        name="demoinput"
                        rules={[
                          {
                            required: true,
                            message: 'Please input anything you like!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        label="Label"
                        name="demoinput"
                        rules={[
                          {
                            required: true,
                            message: 'Please input anything you like!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={12}>
                      <Form.Item
                        label="Label"
                        name="demoinput"
                        rules={[
                          {
                            required: true,
                            message: 'Please input anything you like!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        label="Label"
                        name="demoinput"
                        rules={[
                          {
                            required: true,
                            message: 'Please input anything you like!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={12}>
                      <Form.Item
                        label="Label"
                        name="demoinput"
                        rules={[
                          {
                            required: true,
                            message: 'Please input anything you like!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        label="Label"
                        name="demoinput"
                        rules={[
                          {
                            required: true,
                            message: 'Please input anything you like!',
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={12}></Col>
                    <Col span={12}>
                      <Form.Item>
                        <Button type="primary" htmlType="submit">Submit</Button>
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </div>
            </div>
        );
    };
}

export default RequestForm;